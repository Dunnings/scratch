/// <reference path="../references.ts" />

namespace Scratch.Components
{
    export class ScratchCard extends PIXI.Container
    {
        protected _settings: IScratchCardSettings; //Stores the settings for this scratch card instance

        protected _allRevealed: boolean = false; //If true, all cells are revealed
        protected _mouseDown: boolean = false; //If true, the mouse is currently down
        protected _timeLastCheckedReveal: number = 0; //The time elapsed since the start of the application, when the last reveal check happened

        protected _brush: PIXI.DisplayObject; //Instance of the brush used to reveal cells
        protected _revealButton: PIXI.Text; //Instance of the button used to reveal all cells instantly
        
        protected _cells: Cell[]; //Array containing all child cells

        protected _completeCallback: ()=>void; //Callback called when all cells are revealed

        constructor(settings: IScratchCardSettings, completeCallback: ()=>void)
        {
            super();
            this._settings = settings;
            this._completeCallback = completeCallback;

            this.addListeners();
            this.createCells();
            this.createBrush();
            this.createRevealButton();
        }

        //Setup listeners for all necessary pointer events
        protected addListeners()
        {
            Game.Application.renderer.plugins.interaction.on('pointerdown', this.mouseDown, this);
            Game.Application.renderer.plugins.interaction.on('pointermove', this.mouseMove, this);
            Game.Application.renderer.plugins.interaction.on('pointerup',   this.mouseUp,   this);
        }

        //Create the child cells from given settings
        protected createCells()
        {
            this._cells = []; //Initialise the array
            for(let y = 0; y < this._settings.rowCount; y++)
            {
                for(let x = 0; x < this._settings.columnCount; x++)
                {
                    const value: number = Math.floor(Math.random() * 10 + 1); //Create the value for this cell (completely arbitary)
                    const isWinner: boolean = Math.random() > 0.85; //15% chance the cell is awarded

                    let newCell = new Cell(this._settings.cellSettings, value, isWinner, this);
                    newCell.x = x * (this._settings.cellSettings.width + this._settings.cellPadding.x);
                    newCell.y = y * (this._settings.cellSettings.height + this._settings.cellPadding.y);

                    this._cells.push(newCell);
                    this.addChild(newCell);
                }
            }
        }
        
        //Create an instance of a brush from given settings
        protected createBrush()
        {
            if (this._settings.brushSettings.useTexture && this._settings.brushSettings.textureLocation)
            {
                let texture = PIXI.Texture.fromImage(this._settings.brushSettings.textureLocation);
                this._brush = new PIXI.Sprite(texture);
                this._brush.scale = this._settings.brushSettings.textureScale || new PIXI.Point(1, 1);
                if (texture.baseTexture.hasLoaded) {
                    this._brush.pivot.set(texture.width / 2, texture.height / 2);
                } 
                else {       
                    texture.baseTexture.on('loaded', ()=>{
                        this._brush.pivot.set(texture.width / 2, texture.height / 2);
                    });
                }
            }
            else
            {
                let newBrush: PIXI.Graphics = new PIXI.Graphics();
                newBrush.beginFill(0x000000, 1.0);
                newBrush.drawCircle(0, 0, this._settings.brushSettings.circleRadius || 0);
                newBrush.endFill();
                this._brush = newBrush;
            }
        }

        //Create the reveal button which instantly reveals all cells
        protected createRevealButton()
        {
            this._revealButton = new PIXI.Text("REVEAL ALL", Config.GameSettings.defaultGameFontStyle);
            this._revealButton.pivot.x = this._revealButton.width / 2;
            this._revealButton.x = this.width / 2;
            this._revealButton.y = this.height + 5;
            this.addChild(this._revealButton);

            this._revealButton.interactive = true;
            this._revealButton.on("pointerdown", this.revealAllPressed, this);
        }
        
        //The reveal all button has been pressed
        protected revealAllPressed(e: PIXI.interaction.InteractionEvent)
        {
            if (this._allRevealed) { return; }

            e.stopPropagation();
            this.revealAll();
        }

        //Called by a cell when it's been revealed (would be replace by an event system in a real engine)
        public cellRevealed()
        {
            const allCellsRevealed = this._cells.every((cell)=>{ return cell.isRevealed; });
            if (!this._allRevealed && allCellsRevealed)
            {
                this.revealAll();
            }
        }

        //Iterate through all cells and reveal them, then call the complete callback
        protected revealAll()
        {
            if (this._allRevealed) { return; }

            this._allRevealed = true;
            this._cells.forEach((cell)=>{
                if(!cell.isRevealed)
                {
                    cell.reveal();
                }
            });
            this.toggleRevealButton(false);
            this._completeCallback();
        }
        
        //Used to enable/disable the reveal all button
        protected toggleRevealButton(enabled: boolean)
        {
            this._revealButton.interactive = enabled;
            this._revealButton.alpha = enabled?1.0:0.2;
        }
        
        //Update mousedown state on mousedown and call mousemove
        public mouseDown(e: PIXI.interaction.InteractionEvent)
        {
            this._mouseDown = true;
            this.mouseMove(e);
        }
        
        //Update mousedown state on mouse up
        public mouseUp(e: PIXI.interaction.InteractionEvent)
        {
            this._mouseDown = false;
        }
        
        //When the mouse moves and it is currently pressed down, update cell masks and possibly check percentages
        public mouseMove(e: PIXI.interaction.InteractionEvent)
        {
            if (this._mouseDown)
            {
                let shouldCheck = false;
                if(Game.TimeElapsedMS - this._timeLastCheckedReveal > this._settings.revealCheckDelay)
                {
                    this._timeLastCheckedReveal = Game.TimeElapsedMS;
                    shouldCheck = true;
                }
                
                this._cells.forEach((cell)=>{
                    cell.drawOnMask(e.data.global, this._brush);
                    if (shouldCheck)
                    {
                        cell.checkPercentage();
                    }
                });
            }
        }

        //Called when this instance is destroyed
        public destroy()
        {
            this._cells.forEach(element => {
                element.destroy();
                this.removeChild(element);
            });
        }
    }
}