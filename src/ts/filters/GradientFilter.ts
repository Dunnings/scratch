/// <reference path="../references.ts" />

namespace Scratch.Filters
{
    //Quick little filter to add a gradient to the cells
    export class GradientFilter extends PIXI.Filter<{}>
    {
        constructor(topColor: number[], bottomColor: number[])
        {
            super(undefined, [
                "precision lowp float;",
                "uniform sampler2D uSampler;",
                "uniform vec4 topColor;",
                "uniform vec4 bottomColor;",
                "varying vec2 vTextureCoord;",
                "void main(void){",
                "    vec4 inColor = texture2D(uSampler, vTextureCoord);",
                "    vec4 outputCol = mix(topColor, bottomColor, vTextureCoord.y);",
                "    outputCol = vec4(outputCol.rgb * inColor.rgb, inColor.a);",
                "    gl_FragColor = outputCol;",
                "}"
            ].join('\n'));

            this.uniforms["topColor"] = topColor;
            this.uniforms["bottomColor"] = bottomColor;
        }
    }
}