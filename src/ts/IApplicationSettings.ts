/// <reference path="references.ts" />

namespace Scratch
{
    export interface IApplicationSettings
    {
        width: number, //Width of the game
        height: number //Height of the game
    }
}