/// <reference path="../references.ts" />

namespace Scratch.Components
{
    export interface IScratchCardSettings
    {
        columnCount: number, //Number of columns of cells
        rowCount: number, //Number of rows of cells
        cellPadding: PIXI.Point, //Distance between cells
        cellSettings: ICellSettings, //Appearance settings for each cell
        brushSettings: IBrushSettings, //The appearance of the brush used to reveal the scratch card
        revealCheckDelay: number //This is used to improve performance by setting a minimum time between percentage checks
    }
}