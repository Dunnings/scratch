/// <reference path="../references.ts" />

namespace Scratch.Config
{
    export const GameSettings: Scratch.IGameSettings =
    {
        width: 800,
        height: 500,
        backgroundColor: 0xC49157,
        currencySymbol: "$",
        decimalPlaces: 2,
        defaultGameFontStyle: {fontFamily: 'Baloo Bhaijaan', fontWeight: "bold", fill: 0xFFFFFF},
        versionNumber: "v2.0.0"
    }
}