/// <reference path="references.ts" />

namespace Scratch
{
    export class Game extends PIXIApplication
    {
        protected _scratchCard: Components.ScratchCard; //The currently active scratch card
        
        protected _nextCardButton: PIXI.Text; //Button to load the next scratch card

        constructor()
        {
            super();

            this.createBackground();
            this.createScratchCard();
            this.createNextButton();
            this.createDescription();
            this.createVersionNumber();
        }

        //Create the game background
        protected createBackground()
        {
            let background = new PIXI.Graphics;
            background.beginFill(Config.GameSettings.backgroundColor, 1.0);
            background.drawRect(0, 0, Config.GameSettings.width, Config.GameSettings.height);
            background.endFill();
            Game.Application.stage.addChild(background);
        }

        //Create a new instance of a scratch card
        protected createScratchCard()
        {
            this._scratchCard = new Components.ScratchCard(Config.ScratchCardSettings, ()=>{this.toggleNextButton(true);});
            this._scratchCard.x = 350;
            this._scratchCard.y = 50;
            Game.Application.stage.addChild(this._scratchCard);
        }

        //Create short demo description
        protected createDescription()
        {
            let title = new PIXI.Text("SCRATCH DEMO", Object.assign({fontSize: 32}, Config.GameSettings.defaultGameFontStyle));
            title.x = 10;
            title.y = 80;
            title.pivot.y = title.height;
            Game.Application.stage.addChild(title);

            const descText = "This scratchcard demo was created by \nDavid Dunnings. Source code is hosted on \nbitbucket via the URL provided along with \nthis demo. \nClick 'NEXT CARD' to load a new scratchcard.";
            let description = new PIXI.Text(descText, Object.assign({fontSize: 16}, Config.GameSettings.defaultGameFontStyle));
            description.x = 10;
            description.y = 90;
            Game.Application.stage.addChild(description);
        }

        //Create version number
        protected createVersionNumber()
        {
            let verNumber = new PIXI.Text(Scratch.Config.GameSettings.versionNumber, Object.assign({fontSize: 14}, Config.GameSettings.defaultGameFontStyle));
            verNumber.x = 5;
            verNumber.y = 2;
            Game.Application.stage.addChild(verNumber);
        }
        
        //Create the button that loads the next scratch card
        protected createNextButton()
        {
            this._nextCardButton = new PIXI.Text("NEXT CARD", Object.assign({fontSize: 32}, Config.GameSettings.defaultGameFontStyle));
            this._nextCardButton.x = 85;
            this._nextCardButton.y = 460;
            this._nextCardButton.pivot.y = this._nextCardButton.height;
            Game.Application.stage.addChild(this._nextCardButton);

            this._nextCardButton.interactive = true;
            this._nextCardButton.on("pointerup", this.nextCard, this);

            this.toggleNextButton(false);
        }

        //Toggle the interactive state of the next button
        protected toggleNextButton(enabled: boolean)
        {
            this._nextCardButton.interactive = enabled;
            this._nextCardButton.alpha = enabled?1.0:0.2;
        }

        //Remove the current scratch card and add a new one
        protected nextCard()
        {
            this._scratchCard.destroy();
            Game.Application.stage.removeChild(this._scratchCard);
            this.createScratchCard();
            this.toggleNextButton(false);
        }
    }
}