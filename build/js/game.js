"use strict";
/// <reference path="references.ts" />
var Scratch;
(function (Scratch) {
    class PIXIApplication {
        constructor() {
            this.createApp();
            requestAnimationFrame(this.renderLoop.bind(this));
        }
        //Initialise the PIXI application
        createApp() {
            Scratch.Game.Application = new PIXI.Application(Scratch.Config.GameSettings.width, Scratch.Config.GameSettings.height);
            Scratch.Game.Application.renderer.view.style.position = 'absolute';
            Scratch.Game.Application.renderer.view.style.top = '0px';
            Scratch.Game.Application.renderer.view.style.left = '0px';
            window.addEventListener('resize', this.resize);
            document.body.appendChild(Scratch.Game.Application.view);
            this.resize();
        }
        //Render loop using 'requestAnimationFrame'
        //Also keeps track of total time elapsed
        renderLoop(timestamp) {
            Scratch.Game.Application.render();
            Scratch.Game.TimeElapsedMS = timestamp;
            requestAnimationFrame(this.renderLoop.bind(this));
        }
        //Called when the window is resized
        resize() {
            const maxRatio = window.innerHeight / Scratch.Config.GameSettings.height; //Calculate the maximum the stage can be scaled and still fit in based on height
            const ratio = Math.min(maxRatio, window.innerWidth / Scratch.Config.GameSettings.width); //Calculate the maximum the stage can be scaled based on width and still fit with previous maxRatio
            Scratch.Game.Application.stage.scale.x = Scratch.Game.Application.stage.scale.y = ratio; //Update scales
            Scratch.Game.Application.renderer.resize(ratio * Scratch.Config.GameSettings.width, ratio * Scratch.Config.GameSettings.height); //Maximize renderer to window size
        }
    }
    Scratch.PIXIApplication = PIXIApplication;
})(Scratch || (Scratch = {}));
/// <reference path="../references.ts" />
var Scratch;
(function (Scratch) {
    var Components;
    (function (Components) {
        class Cell extends PIXI.Container {
            constructor(settings, value, isWinner, parent) {
                super();
                this._revealed = false; //If true, this cell has been completely revealed
                this._settings = settings;
                this._parent = parent;
                this._winner = isWinner;
                this.createBackground();
                this.createValue(value);
                this.createLabel();
                this.createForeground();
                this.createForegroundMask();
            }
            //Public getter used to query the revealed state of the cell
            get isRevealed() {
                return this._revealed;
            }
            //Create the cell background
            createBackground() {
                let background = new PIXI.Graphics();
                background.beginFill(this._settings.backgroundColor);
                background.drawRoundedRect(0, 0, this._settings.width, this._settings.height, this._settings.roundedCornerWidth);
                background.endFill();
                this.addChild(background);
            }
            //Create the value text element
            createValue(value) {
                const valueContents = Scratch.Config.GameSettings.currencySymbol + value.toFixed(Scratch.Config.GameSettings.decimalPlaces);
                this._valueText = new PIXI.Text(valueContents, this._settings.valueTextSettings);
                this._valueText.pivot.y = this._valueText.height / 2;
                this._valueText.pivot.x = this._valueText.width / 2;
                this._valueText.y = this._settings.height / 2 - 10;
                this._valueText.x = this._settings.width / 2;
                this.addChild(this._valueText);
                if (this._winner) {
                    this._valueText.style.fill = this._settings.winningTextColor;
                }
            }
            //Create the label text element
            createLabel() {
                let value = this._winner ? "WIN" : "NO WIN";
                this._labelText = new PIXI.Text(value, this._settings.labelTextSettings);
                this._labelText.pivot.y = this._labelText.height / 2;
                this._labelText.pivot.x = this._labelText.width / 2;
                this._labelText.y = this._settings.height - 25;
                this._labelText.x = this._settings.width / 2;
                this.addChild(this._labelText);
                if (this._winner) {
                    this._labelText.style.fill = this._settings.winningTextColor;
                }
            }
            //Create the cell foreground (foil)
            createForeground() {
                this._foreground = new PIXI.Graphics();
                this._foreground.beginFill(this._settings.foregroundColor);
                this._foreground.drawRoundedRect(0, 0, this._settings.width, this._settings.height, this._settings.roundedCornerWidth);
                this._foreground.endFill();
                this.addChild(this._foreground);
                //Add a subtle linear gradient
                this._foreground.filters = [new Scratch.Filters.GradientFilter([0.8, 0.8, 0.8, 1.0], [1.0, 1.0, 1.0, 1.0])];
            }
            //Create the mask for the cell foreground
            createForegroundMask() {
                let whiteMask = new PIXI.Graphics();
                whiteMask.beginFill(0xffffff, 1.0);
                whiteMask.drawRoundedRect(0, 0, this._settings.width, this._settings.height, this._settings.roundedCornerWidth);
                whiteMask.endFill();
                this._renderTex = PIXI.RenderTexture.create(this._settings.width, this._settings.height);
                let renderTexSprite = new PIXI.Sprite(this._renderTex); //In order to use a render texture as a mask it must first be given to a Sprite
                this._foreground.mask = renderTexSprite; //The sprite can then be used as a mask
                this.addChild(renderTexSprite);
                Scratch.Game.Application.renderer.render(whiteMask, this._renderTex); //Render all white to the render texture
            }
            //Use the brush to update the mask
            drawOnMask(position, brush) {
                if (this._revealed) {
                    return;
                }
                position = this.toLocal(position);
                brush.x = position.x;
                brush.y = position.y;
                Scratch.Game.Application.renderer.render(brush, this._renderTex, false); //Render the brush to the render texture with clear flag set to false
            }
            //Check if the cell has reached minimum reveal before total reveal
            checkPercentage() {
                if (this._revealed) {
                    return;
                }
                let pixels = Scratch.Game.Application.renderer.plugins.extract.pixels(this._renderTex); //Use the extractor plugin to grab the pixel data from the render texture
                let totalWhitePixels = 0; //Keep cound of the number of white pixels
                pixels.forEach((value) => { if (value == 255) {
                    totalWhitePixels++;
                } }); //Iterate through each pixel and if white, update 'totalWhitePixels'
                if (totalWhitePixels < pixels.length * this._settings.percentToReveal) {
                    this.reveal(); //Reveal the cell completely
                }
            }
            //Reveal the entire cell
            reveal() {
                this._revealed = true; //Update the state boolean
                createjs.Tween.get(this._foreground).to({ alpha: 0 }, 500, createjs.Ease.sineOut); //Ease out the remaining foreground
                if (this._winner) {
                    this.playWinEffect();
                }
                this._parent.cellRevealed(); //Notify the parent scratch card that this cell has been revealed
            }
            //Extra effect for winning cells
            playWinEffect() {
                createjs.Tween.get(this._valueText.scale).wait(250).to({ x: 1.5, y: 1.5 }, 300, createjs.Ease.sineOut).to({ x: 1, y: 1, }, 200, createjs.Ease.sineOut);
                createjs.Tween.get(this._valueText).wait(200).to({ rotation: 0.6 }, 200, createjs.Ease.sineOut).to({ rotation: -0.6 }, 200, createjs.Ease.sineOut).to({ rotation: 0 }, 100, createjs.Ease.sineOut);
            }
            //Called when this instance is destroyed
            destroy() {
                this._renderTex.destroy();
                this.removeChildren();
            }
        }
        Components.Cell = Cell;
    })(Components = Scratch.Components || (Scratch.Components = {}));
})(Scratch || (Scratch = {}));
/// <reference path="../references.ts" />
var Scratch;
(function (Scratch) {
    var Components;
    (function (Components) {
        class ScratchCard extends PIXI.Container {
            constructor(settings, completeCallback) {
                super();
                this._allRevealed = false; //If true, all cells are revealed
                this._mouseDown = false; //If true, the mouse is currently down
                this._timeLastCheckedReveal = 0; //The time elapsed since the start of the application, when the last reveal check happened
                this._settings = settings;
                this._completeCallback = completeCallback;
                this.addListeners();
                this.createCells();
                this.createBrush();
                this.createRevealButton();
            }
            //Setup listeners for all necessary pointer events
            addListeners() {
                Scratch.Game.Application.renderer.plugins.interaction.on('pointerdown', this.mouseDown, this);
                Scratch.Game.Application.renderer.plugins.interaction.on('pointermove', this.mouseMove, this);
                Scratch.Game.Application.renderer.plugins.interaction.on('pointerup', this.mouseUp, this);
            }
            //Create the child cells from given settings
            createCells() {
                this._cells = []; //Initialise the array
                for (let y = 0; y < this._settings.rowCount; y++) {
                    for (let x = 0; x < this._settings.columnCount; x++) {
                        const value = Math.floor(Math.random() * 10 + 1); //Create the value for this cell (completely arbitary)
                        const isWinner = Math.random() > 0.85; //15% chance the cell is awarded
                        let newCell = new Components.Cell(this._settings.cellSettings, value, isWinner, this);
                        newCell.x = x * (this._settings.cellSettings.width + this._settings.cellPadding.x);
                        newCell.y = y * (this._settings.cellSettings.height + this._settings.cellPadding.y);
                        this._cells.push(newCell);
                        this.addChild(newCell);
                    }
                }
            }
            //Create an instance of a brush from given settings
            createBrush() {
                if (this._settings.brushSettings.useTexture && this._settings.brushSettings.textureLocation) {
                    let texture = PIXI.Texture.fromImage(this._settings.brushSettings.textureLocation);
                    this._brush = new PIXI.Sprite(texture);
                    this._brush.scale = this._settings.brushSettings.textureScale || new PIXI.Point(1, 1);
                    if (texture.baseTexture.hasLoaded) {
                        this._brush.pivot.set(texture.width / 2, texture.height / 2);
                    }
                    else {
                        texture.baseTexture.on('loaded', () => {
                            this._brush.pivot.set(texture.width / 2, texture.height / 2);
                        });
                    }
                }
                else {
                    let newBrush = new PIXI.Graphics();
                    newBrush.beginFill(0x000000, 1.0);
                    newBrush.drawCircle(0, 0, this._settings.brushSettings.circleRadius || 0);
                    newBrush.endFill();
                    this._brush = newBrush;
                }
            }
            //Create the reveal button which instantly reveals all cells
            createRevealButton() {
                this._revealButton = new PIXI.Text("REVEAL ALL", Scratch.Config.GameSettings.defaultGameFontStyle);
                this._revealButton.pivot.x = this._revealButton.width / 2;
                this._revealButton.x = this.width / 2;
                this._revealButton.y = this.height + 5;
                this.addChild(this._revealButton);
                this._revealButton.interactive = true;
                this._revealButton.on("pointerdown", this.revealAllPressed, this);
            }
            //The reveal all button has been pressed
            revealAllPressed(e) {
                if (this._allRevealed) {
                    return;
                }
                e.stopPropagation();
                this.revealAll();
            }
            //Called by a cell when it's been revealed (would be replace by an event system in a real engine)
            cellRevealed() {
                const allCellsRevealed = this._cells.every((cell) => { return cell.isRevealed; });
                if (!this._allRevealed && allCellsRevealed) {
                    this.revealAll();
                }
            }
            //Iterate through all cells and reveal them, then call the complete callback
            revealAll() {
                if (this._allRevealed) {
                    return;
                }
                this._allRevealed = true;
                this._cells.forEach((cell) => {
                    if (!cell.isRevealed) {
                        cell.reveal();
                    }
                });
                this.toggleRevealButton(false);
                this._completeCallback();
            }
            //Used to enable/disable the reveal all button
            toggleRevealButton(enabled) {
                this._revealButton.interactive = enabled;
                this._revealButton.alpha = enabled ? 1.0 : 0.2;
            }
            //Update mousedown state on mousedown and call mousemove
            mouseDown(e) {
                this._mouseDown = true;
                this.mouseMove(e);
            }
            //Update mousedown state on mouse up
            mouseUp(e) {
                this._mouseDown = false;
            }
            //When the mouse moves and it is currently pressed down, update cell masks and possibly check percentages
            mouseMove(e) {
                if (this._mouseDown) {
                    let shouldCheck = false;
                    if (Scratch.Game.TimeElapsedMS - this._timeLastCheckedReveal > this._settings.revealCheckDelay) {
                        this._timeLastCheckedReveal = Scratch.Game.TimeElapsedMS;
                        shouldCheck = true;
                    }
                    this._cells.forEach((cell) => {
                        cell.drawOnMask(e.data.global, this._brush);
                        if (shouldCheck) {
                            cell.checkPercentage();
                        }
                    });
                }
            }
            //Called when this instance is destroyed
            destroy() {
                this._cells.forEach(element => {
                    element.destroy();
                    this.removeChild(element);
                });
            }
        }
        Components.ScratchCard = ScratchCard;
    })(Components = Scratch.Components || (Scratch.Components = {}));
})(Scratch || (Scratch = {}));
/// <reference path="../references.ts" />
var Scratch;
(function (Scratch) {
    var Config;
    (function (Config) {
        Config.GameSettings = {
            width: 800,
            height: 500,
            backgroundColor: 0xC49157,
            currencySymbol: "$",
            decimalPlaces: 2,
            defaultGameFontStyle: { fontFamily: 'Baloo Bhaijaan', fontWeight: "bold", fill: 0xFFFFFF },
            versionNumber: "v2.0.0"
        };
    })(Config = Scratch.Config || (Scratch.Config = {}));
})(Scratch || (Scratch = {}));
/// <reference path="../references.ts" />
var Scratch;
(function (Scratch) {
    var Config;
    (function (Config) {
        Config.ScratchCardSettings = {
            columnCount: 3,
            rowCount: 3,
            cellPadding: new PIXI.Point(10, 10),
            cellSettings: {
                width: 128,
                height: 128,
                roundedCornerWidth: 8,
                backgroundColor: 0xFFFFFF,
                foregroundColor: 0xFFFFFF,
                valueTextSettings: {
                    fontFamily: "Baloo Bhaijaan",
                    align: "center",
                    fontSize: 32,
                    fill: 0xe60201,
                    fontWeight: "bold"
                },
                labelTextSettings: {
                    fontFamily: "Baloo Bhaijaan",
                    align: "center",
                    fontSize: 24,
                    fill: 0xe60201,
                    fontWeight: "bold"
                },
                winningTextColor: 0x66d62a,
                percentToReveal: 0.5
            },
            brushSettings: {
                useTexture: true,
                circleRadius: 45,
                textureLocation: "img/brush.png",
                textureScale: new PIXI.Point(0.9, 0.9)
            },
            revealCheckDelay: 500
        };
    })(Config = Scratch.Config || (Scratch.Config = {}));
})(Scratch || (Scratch = {}));
/// <reference path="../references.ts" />
var Scratch;
(function (Scratch) {
    var Filters;
    (function (Filters) {
        //Quick little filter to add a gradient to the cells
        class GradientFilter extends PIXI.Filter {
            constructor(topColor, bottomColor) {
                super(undefined, [
                    "precision lowp float;",
                    "uniform sampler2D uSampler;",
                    "uniform vec4 topColor;",
                    "uniform vec4 bottomColor;",
                    "varying vec2 vTextureCoord;",
                    "void main(void){",
                    "    vec4 inColor = texture2D(uSampler, vTextureCoord);",
                    "    vec4 outputCol = mix(topColor, bottomColor, vTextureCoord.y);",
                    "    outputCol = vec4(outputCol.rgb * inColor.rgb, inColor.a);",
                    "    gl_FragColor = outputCol;",
                    "}"
                ].join('\n'));
                this.uniforms["topColor"] = topColor;
                this.uniforms["bottomColor"] = bottomColor;
            }
        }
        Filters.GradientFilter = GradientFilter;
    })(Filters = Scratch.Filters || (Scratch.Filters = {}));
})(Scratch || (Scratch = {}));
/// <reference path="references.ts" />
/// <reference path="references.ts" />
/// <reference path="../references.ts" />
/// <reference path="../references.ts" />
/// <reference path="../references.ts" />
//Classes
/// <reference path="Game.ts" />
/// <reference path="PIXIApplication.ts" />
/// <reference path="components/Cell.ts" />
/// <reference path="components/ScratchCard.ts" />
/// <reference path="config/GameSettings.ts" />
/// <reference path="config/ScratchCardSettings.ts" />
/// <reference path="filters/GradientFilter.ts" />
//Interfaces
/// <reference path="IApplicationSettings.ts" />
/// <reference path="IGameSettings.ts" />
/// <reference path="components/IBrushSettings.ts" />
/// <reference path="components/ICellSettings.ts" />
/// <reference path="components/IScratchCardSettings.ts" /> 
/// <reference path="references.ts" />
var Scratch;
(function (Scratch) {
    class Game extends Scratch.PIXIApplication {
        constructor() {
            super();
            this.createBackground();
            this.createScratchCard();
            this.createNextButton();
            this.createDescription();
            this.createVersionNumber();
        }
        //Create the game background
        createBackground() {
            let background = new PIXI.Graphics;
            background.beginFill(Scratch.Config.GameSettings.backgroundColor, 1.0);
            background.drawRect(0, 0, Scratch.Config.GameSettings.width, Scratch.Config.GameSettings.height);
            background.endFill();
            Game.Application.stage.addChild(background);
        }
        //Create a new instance of a scratch card
        createScratchCard() {
            this._scratchCard = new Scratch.Components.ScratchCard(Scratch.Config.ScratchCardSettings, () => { this.toggleNextButton(true); });
            this._scratchCard.x = 350;
            this._scratchCard.y = 50;
            Game.Application.stage.addChild(this._scratchCard);
        }
        //Create short demo description
        createDescription() {
            let title = new PIXI.Text("SCRATCH DEMO", Object.assign({ fontSize: 32 }, Scratch.Config.GameSettings.defaultGameFontStyle));
            title.x = 10;
            title.y = 80;
            title.pivot.y = title.height;
            Game.Application.stage.addChild(title);
            const descText = "This scratchcard demo was created by \nDavid Dunnings. Source code is hosted on \nbitbucket via the URL provided along with \nthis demo. \nClick 'NEXT CARD' to load a new scratchcard.";
            let description = new PIXI.Text(descText, Object.assign({ fontSize: 16 }, Scratch.Config.GameSettings.defaultGameFontStyle));
            description.x = 10;
            description.y = 90;
            Game.Application.stage.addChild(description);
        }
        //Create version number
        createVersionNumber() {
            let verNumber = new PIXI.Text(Scratch.Config.GameSettings.versionNumber, Object.assign({ fontSize: 14 }, Scratch.Config.GameSettings.defaultGameFontStyle));
            verNumber.x = 5;
            verNumber.y = 2;
            Game.Application.stage.addChild(verNumber);
        }
        //Create the button that loads the next scratch card
        createNextButton() {
            this._nextCardButton = new PIXI.Text("NEXT CARD", Object.assign({ fontSize: 32 }, Scratch.Config.GameSettings.defaultGameFontStyle));
            this._nextCardButton.x = 85;
            this._nextCardButton.y = 460;
            this._nextCardButton.pivot.y = this._nextCardButton.height;
            Game.Application.stage.addChild(this._nextCardButton);
            this._nextCardButton.interactive = true;
            this._nextCardButton.on("pointerup", this.nextCard, this);
            this.toggleNextButton(false);
        }
        //Toggle the interactive state of the next button
        toggleNextButton(enabled) {
            this._nextCardButton.interactive = enabled;
            this._nextCardButton.alpha = enabled ? 1.0 : 0.2;
        }
        //Remove the current scratch card and add a new one
        nextCard() {
            this._scratchCard.destroy();
            Game.Application.stage.removeChild(this._scratchCard);
            this.createScratchCard();
            this.toggleNextButton(false);
        }
    }
    Scratch.Game = Game;
})(Scratch || (Scratch = {}));
