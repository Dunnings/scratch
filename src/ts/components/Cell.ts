/// <reference path="../references.ts" />

namespace Scratch.Components
{
    export class Cell extends PIXI.Container
    {
        protected _settings: ICellSettings; //Instance of the cell settings

        protected _parent: ScratchCard; //Reference to the cell's parent scratchcard

        protected _foreground: PIXI.Graphics; //Instance of the foreground graphics object (could be replace with a Sprite at a later date)

        protected _renderTex: PIXI.RenderTexture; //Render texture used for masking

        protected _labelText: PIXI.Text; //Instance of the label text element for this cell
        protected _valueText: PIXI.Text; //Instance of the value text element for this cell

        protected _winner: boolean; //If true, this cell is a winning cell
        protected _revealed: boolean = false; //If true, this cell has been completely revealed

        constructor(settings: ICellSettings, value: number, isWinner: boolean, parent: ScratchCard)
        {
            super();
            this._settings = settings;
            this._parent = parent;
            this._winner = isWinner;

            this.createBackground();
            this.createValue(value);
            this.createLabel();
            this.createForeground();
            this.createForegroundMask();
        }

        //Public getter used to query the revealed state of the cell
        public get isRevealed()
        {
            return this._revealed;
        }
        
        //Create the cell background
        protected createBackground()
        {
            let background = new PIXI.Graphics();
            background.beginFill(this._settings.backgroundColor);
            background.drawRoundedRect(0,0,this._settings.width, this._settings.height, this._settings.roundedCornerWidth);
            background.endFill();
            this.addChild(background);
        }

        //Create the value text element
        protected createValue(value: number)
        {
            const valueContents: string = Config.GameSettings.currencySymbol + value.toFixed(Scratch.Config.GameSettings.decimalPlaces);
            this._valueText = new PIXI.Text(valueContents, this._settings.valueTextSettings);
            this._valueText.pivot.y = this._valueText.height / 2;
            this._valueText.pivot.x = this._valueText.width / 2;
            this._valueText.y = this._settings.height / 2 - 10;
            this._valueText.x = this._settings.width / 2;
            this.addChild(this._valueText);

            if (this._winner)
            {
                this._valueText.style.fill = this._settings.winningTextColor;
            }
        }

        //Create the label text element
        protected createLabel()
        {
            let value = this._winner?"WIN":"NO WIN";
            this._labelText = new PIXI.Text(value, this._settings.labelTextSettings);
            this._labelText.pivot.y = this._labelText.height / 2;
            this._labelText.pivot.x = this._labelText.width / 2;
            this._labelText.y = this._settings.height - 25;
            this._labelText.x = this._settings.width / 2;
            this.addChild(this._labelText);
            
            if (this._winner)
            {
                this._labelText.style.fill = this._settings.winningTextColor;
            }
        }

        //Create the cell foreground (foil)
        protected createForeground()
        {
            this._foreground = new PIXI.Graphics();
            this._foreground.beginFill(this._settings.foregroundColor);
            this._foreground.drawRoundedRect(0,0,this._settings.width, this._settings.height, this._settings.roundedCornerWidth);
            this._foreground.endFill();
            this.addChild(this._foreground);

            //Add a subtle linear gradient
            this._foreground.filters = [new Filters.GradientFilter([0.8, 0.8, 0.8, 1.0], [1.0, 1.0, 1.0, 1.0])];
        }

        //Create the mask for the cell foreground
        protected createForegroundMask()
        {
            let whiteMask = new PIXI.Graphics();
            whiteMask.beginFill(0xffffff, 1.0);
            whiteMask.drawRoundedRect(0,0,this._settings.width, this._settings.height, this._settings.roundedCornerWidth);
            whiteMask.endFill();

            this._renderTex = PIXI.RenderTexture.create(this._settings.width, this._settings.height);

            let renderTexSprite = new PIXI.Sprite(this._renderTex); //In order to use a render texture as a mask it must first be given to a Sprite
            this._foreground.mask = renderTexSprite; //The sprite can then be used as a mask
            this.addChild(renderTexSprite);
            
            Game.Application.renderer.render(whiteMask, this._renderTex); //Render all white to the render texture
        }

        //Use the brush to update the mask
        public drawOnMask(position: PIXI.Point, brush: PIXI.DisplayObject)
        {
            if(this._revealed){ return; }

            position = this.toLocal(position);
            brush.x = position.x;
            brush.y = position.y;
            Game.Application.renderer.render(brush, this._renderTex, false); //Render the brush to the render texture with clear flag set to false
        }

        //Check if the cell has reached minimum reveal before total reveal
        public checkPercentage()
        {
            if(this._revealed){ return; }
            
            let pixels: number[] = Game.Application.renderer.plugins.extract.pixels(this._renderTex); //Use the extractor plugin to grab the pixel data from the render texture
            let totalWhitePixels = 0; //Keep cound of the number of white pixels
            pixels.forEach((value)=>{if(value==255){totalWhitePixels++;}}); //Iterate through each pixel and if white, update 'totalWhitePixels'
            if (totalWhitePixels < pixels.length * this._settings.percentToReveal) //If the number of white pixels exceeds the percentage in the settings
            {
                this.reveal(); //Reveal the cell completely
            }
        }
        
        //Reveal the entire cell
        public reveal()
        {
            this._revealed = true; //Update the state boolean
            createjs.Tween.get(this._foreground).to({alpha: 0}, 500, createjs.Ease.sineOut); //Ease out the remaining foreground
            if (this._winner)
            {
                this.playWinEffect();
            }
            this._parent.cellRevealed(); //Notify the parent scratch card that this cell has been revealed
        }

        //Extra effect for winning cells
        protected playWinEffect()
        {
            createjs.Tween.get(this._valueText.scale).wait(250).to({x: 1.5, y: 1.5}, 300, createjs.Ease.sineOut).to({x: 1, y: 1,}, 200, createjs.Ease.sineOut);
            createjs.Tween.get(this._valueText).wait(200).to({rotation: 0.6}, 200, createjs.Ease.sineOut).to({rotation: -0.6}, 200, createjs.Ease.sineOut).to({rotation: 0}, 100, createjs.Ease.sineOut);
        }

        //Called when this instance is destroyed
        public destroy()
        {
            this._renderTex.destroy();
            this.removeChildren();
        }
    }
}