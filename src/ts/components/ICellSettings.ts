/// <reference path="../references.ts" />

namespace Scratch.Components
{
    export interface ICellSettings
    {
        width: number, //Cell width
        height: number, //Cell height
        roundedCornerWidth: number, //Roundness of cell corners
        backgroundColor: number, //Background color of the cell
        foregroundColor: number, //Foreground color of the cell (the foil)
        valueTextSettings: PIXI.TextStyleOptions, //Style settings for value text
        labelTextSettings: PIXI.TextStyleOptions, //Style settings for label text
        winningTextColor: number, //Color override for winning cells
        percentToReveal: number //How much of a cell must be visible before revealing the whole cell (set to 1.0 to never auto reveal)
    }
}