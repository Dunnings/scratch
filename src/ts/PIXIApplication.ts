/// <reference path="references.ts" />

namespace Scratch
{
    export class PIXIApplication
    {
        public static Application: PIXI.Application; //Globaly available reference to the PIXI application (not generally a good idea, used here for simplicity)
        public static TimeElapsedMS: number; //Total time the game has been running

        constructor()
        {
            this.createApp();

            requestAnimationFrame(this.renderLoop.bind(this));
        }

        //Initialise the PIXI application
        protected createApp()
        {
            Game.Application = new PIXI.Application(Config.GameSettings.width, Config.GameSettings.height);
            Game.Application.renderer.view.style.position = 'absolute';
            Game.Application.renderer.view.style.top = '0px';
            Game.Application.renderer.view.style.left = '0px';
            window.addEventListener('resize', this.resize);
            document.body.appendChild(Game.Application.view);

            this.resize();
        }

        //Render loop using 'requestAnimationFrame'
        //Also keeps track of total time elapsed
        protected renderLoop(timestamp: number)
        {
            Game.Application.render();
            Game.TimeElapsedMS = timestamp;
            requestAnimationFrame(this.renderLoop.bind(this));
        }

        //Called when the window is resized
        protected resize()
        {
            const maxRatio = window.innerHeight / Config.GameSettings.height; //Calculate the maximum the stage can be scaled and still fit in based on height
            const ratio = Math.min(maxRatio, window.innerWidth / Config.GameSettings.width); //Calculate the maximum the stage can be scaled based on width and still fit with previous maxRatio
            Game.Application.stage.scale.x = Game.Application.stage.scale.y = ratio; //Update scales
            Game.Application.renderer.resize(ratio * Config.GameSettings.width, ratio * Config.GameSettings.height); //Maximize renderer to window size
        }
    }
}