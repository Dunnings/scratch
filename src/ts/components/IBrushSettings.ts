/// <reference path="../references.ts" />

namespace Scratch.Components
{
    export interface IBrushSettings
    {
        useTexture: boolean, //If true, a given texture is used for the brush. If false, a graphics circle is used instead
        textureLocation?: string, //Location of the texture
        textureScale?: PIXI.Point, //Scale of the texture
        circleRadius?: number, //Radius of the circle if not using a texture
    }
}