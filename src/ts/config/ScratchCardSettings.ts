/// <reference path="../references.ts" />

namespace Scratch.Config
{
    export const ScratchCardSettings: Scratch.Components.IScratchCardSettings =
    {
        columnCount: 3,
        rowCount: 3,
        cellPadding: new PIXI.Point(10, 10),
        cellSettings: {
            width: 128,
            height: 128,
            roundedCornerWidth: 8,
            backgroundColor: 0xFFFFFF,
            foregroundColor: 0xFFFFFF,
            valueTextSettings: {
                fontFamily: "Baloo Bhaijaan",
                align: "center",
                fontSize: 32,
                fill: 0xe60201,
                fontWeight: "bold"
            },
            labelTextSettings: {
                fontFamily: "Baloo Bhaijaan",
                align: "center",
                fontSize: 24,
                fill: 0xe60201,
                fontWeight: "bold"
            },
            winningTextColor: 0x66d62a,
            percentToReveal: 0.5
        },
        brushSettings: {
            useTexture: true,
            circleRadius: 45,
            textureLocation: "img/brush.png",
            textureScale: new PIXI.Point(0.9, 0.9)
        },
        revealCheckDelay: 500
    }
}