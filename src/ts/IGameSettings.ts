/// <reference path="references.ts" />

namespace Scratch
{
    export interface IGameSettings extends IApplicationSettings
    {
        backgroundColor: number, //Background color of the game
        currencySymbol: string, //Currency symbol displayed in game
        decimalPlaces: number, //How many decimal places should currency values be displayed to
        defaultGameFontStyle: PIXI.TextStyleOptions, //Default font style for the game
        versionNumber: string //Current iteration to be displayed in game
    }
}